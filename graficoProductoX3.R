#Bucle que crea el Array de Años
YearsNum<-c()
for (i in YearIni:YearFin){
  YearsNum<-c(YearsNum, i)
}

MesesNum<-c(1,2,3,4,5,6,7,8,9,10,11,12)
#Datos <- DatosX3
Datos <- Datos[!is.na(Datos$Modulo),]
Datos <- Datos[!is.na(Datos$Fecha.Renovacion),]

if (TipoRiesgo == "Predicion"){
  Datos <- Datos[Datos$PredicionBaja == "Baja",]
}

if (TipoRiesgo == "BajaSage"){
  Datos <- Datos[Datos$BajasSAGE == "Baja",]
}

DatosModulo<-Datos
DatosModulo$Modulo<-as.character(DatosModulo$Modulo)

for (i in 1:length(DatosModulo$Modulo)){
  if (grepl("Abel", as.character(DatosModulo$Modulo[i]), ignore.case = TRUE)){
    DatosModulo$Modulo[i]<-"Abel"
  }
  if (grepl("ERPX3", as.character(DatosModulo$Modulo[i]), ignore.case = TRUE)){
    DatosModulo$Modulo[i]<-"ERPX3"
  }
  if (grepl("Universe", as.character(DatosModulo$Modulo[i]), ignore.case = TRUE)){
    DatosModulo$Modulo[i]<-"Universe"
  }
}

for (i in 1:length(DatosModulo$Modulo)){
  if (!(grepl("Abel", as.character(DatosModulo$Modulo[i]), ignore.case = TRUE)) &
      !(grepl("ERPX3", as.character(DatosModulo$Modulo[i]), ignore.case = TRUE)) &
      !(grepl("Universe", as.character(DatosModulo$Modulo[i]), ignore.case = TRUE))){
    DatosModulo$Modulo[i]<-"Others"
  }
}

#Creamos un DataFrame que contendra la Facturacion Total por mes y año
TablaFacturacionFecha<-as.data.frame(matrix(0,nr=length(YearsNum),nc=length(MesesNum)),row.names = as.character(YearsNum))
names(TablaFacturacionFecha)<-MesesNum
Abel<-c()
ERPX3<-c()
Universe<-c()
Others<-c()

for (r in 1:4){
  #r<-1
  if (r==1){
    Prod<-"Abel"}
  if (r==2){
    Prod<-"ERPX3"}
  if (r==3){
    Prod<-"Universe"}
  if (r==4){
    Prod<-"Others"}
  DatosProd<-DatosModulo[DatosModulo$Modulo==Prod,]
  FechaPrueba<-as.Date(DatosProd$Fecha.Renovacion, "%d/%m/%Y")
  FechaPrueba<-as.POSIXlt(FechaPrueba, format = "%d/%m/%Y")
  for (i in 1:length(YearsNum)){
    #i<-8
    Years<-YearsNum[i]
    for (j in 1:length(MesesNum)){
      #j<-5
      Mes<-MesesNum[j]
      
      if (TipoGrafico == "Numero"){
        TablaFacturacionFecha[i,j]<-length(as.numeric(as.character(DatosProd$NIF[
          which((as.numeric(format(FechaPrueba, "%m")) == Mes)&
                  (format(FechaPrueba, "%Y") == Years))])))
      }
      
      if (TipoGrafico == "Facturacion"){
        TablaFacturacionFecha[i,j]<-sum(as.numeric(as.character(DatosProd$Facturacion.Total[
          which((as.numeric(format(FechaPrueba, "%m")) == Mes)&
                  (format(FechaPrueba, "%Y") == Years))])))
      }
      
    }
  }
  if (r==1){
    for (l in 1:length(TablaFacturacionFecha[,1])){
      Abel<-c(Abel, as.numeric(as.character(TablaFacturacionFecha[l,])))
    }
  }
  if (r==2){
    for (l in 1:length(TablaFacturacionFecha[,1])){
      ERPX3<-c(ERPX3, as.numeric(as.character(TablaFacturacionFecha[l,])))
    }
  }
  if (r==3){
    for (l in 1:length(TablaFacturacionFecha[,1])){
      Universe<-c(Universe, as.numeric(as.character(TablaFacturacionFecha[l,])))
    }
  }
  if (r==4){
    for (l in 1:length(TablaFacturacionFecha[,1])){
      Others<-c(Others, as.numeric(as.character(TablaFacturacionFecha[l,])))
    }
  }
}
FechaOrigen<-paste(YearIni, 1, 1, sep = "/")
FechaFin<-paste(YearFin, 12, 31, sep = "/")
FechaX<-seq.Date(from = as.Date(FechaOrigen), to = as.Date(FechaFin), by = "month")
plot(FechaX, Abel, type = "l", col = "green", main = TextoMain,
     ylim = c(0, max(Abel, ERPX3, Universe, Others)),
     xlab = TextoXlab, ylab = TextoYlab, axes = FALSE)
axis(2)
axis(1, at=FechaX, labels = format(FechaX, "%m/%Y"))
lines(FechaX, ERPX3, type = "l", col = "blue")
lines(FechaX, Universe, type = "l", col = "red")
lines(FechaX, Others, type = "l", col = "orange")
legend(x= "topleft",y=0.92, legend=c("Abel", "ERPX3", "Universe", "Others"),
       lty = c(1,3), col = c("green", "blue", "red", "orange"), cex = 0.8)