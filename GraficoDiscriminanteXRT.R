VectorDiscriminante<-c()
TablaDiscriminante<-c()
MediaBaja<-c()
MediaNoBaja<-c()
library(car)

DatosTemp <-read.csv("Datos/DatosConFicticiaUnicos7XRT.csv",header=TRUE, sep=";")
#Seleccionamos solo las variables de interes
Datos<-DatosTemp[,c(5,7,8,9,10,11,12,13,14,15,16,19,20,21,22,23,24,25,26,27,40)]
Datos$BajasSAGE <- recode(Datos$BajasSAGE, '0="No Baja"; 1="Baja"', as.factor.result=TRUE)

tryCatch(
  for (i in 1:(ncol(Datos)-1)){
    #i<-15
    VarDis<-c()
    VarDis<-cbind(Datos[,i], Datos[,ncol(Datos)])
    VarDis<-VarDis[!is.na(VarDis[,1]),]
    VarDis<-as.data.frame(VarDis)
    names(VarDis)<-c("Variable", "BajasSAGE")
    VarDis$BajasSAGE<-recode(VarDis$BajasSAGE, "2='No Baja'; 1='Baja'")
    Variable<-VarDis$Variable
    Variable<-as.numeric(as.character(Variable))
  
    if (length(Variable) != 0){
      Test<-t.test(Variable~VarDis$BajasSAGE, alternative='two.sided', conf.level=.95, var.equal=FALSE, data=Datos)
      PValor<-round(Test$p.value,4)
      TituloPValor<-paste(100-PValor*100)
    }else{
      TituloPValor<-0
    }
    MediaBaja<-rbind(MediaBaja, sum(VarDis[VarDis$BajasSAGE == "Baja",1])/length(VarDis[VarDis$BajasSAGE == "Baja",1]))
    MediaNoBaja<-rbind(MediaNoBaja, sum(VarDis[VarDis$BajasSAGE == "No Baja",1])/length(VarDis[VarDis$BajasSAGE == "No Baja",1]))
    VectorDiscriminante<-rbind(VectorDiscriminante,TituloPValor)
  }
)
TablaDiscriminante<-cbind(names(Datos[, 1:length(VectorDiscriminante)]), VectorDiscriminante, MediaBaja, MediaNoBaja)
TablaDiscriminante<-as.data.frame(TablaDiscriminante)
names(TablaDiscriminante)<-c("Variable", "Valor", "MediaBaja", "MediaNoBaja")
TablaDiscriminanteArbol<-TablaDiscriminante[as.numeric(as.character(TablaDiscriminante$Valor))>90,]

write.table(TablaDiscriminante, "Datos/TablaDiscriminanteXRT.csv", 
          sep=";",row.names=FALSE,fileEncoding="UTF-8")