#Recorremos desde 2 hasta el nodo actual
for (k in 2:i){
  #Si no es par ponemos esPar en FALSE
  if (actualNodo%%2!=0 && actualNodo!=1){
    esPar<-FALSE
  }
}
#Si no es par cerramos la rama
if (esPar==FALSE){
  textoJson<-paste(textoJson, ']}', sep = "")
}